# Zablafuňky (sour potatoes)

Disclaimer: There are multiple variants of this recipe, usually involving milk or vinegar - hence the sourness. This variant uses bacon instead.

- ⏲️ Prep time 10 min
- 🍳Cook time: cca 30 min
- 🍽️ Servings: 4

## Ingredients

- 1kg of potatoes
- 50g flour - soft grain
- 1 cup of cooking cream (~12% fat) or thickened cream (~30% fat)
- 6 eggs (4 hardboiled, 2 raw)
- garlic
- bacon
- allspice (aka myrtle pepper), bay leaf
- butter (optional)

## Directions

1. Cut potatoes into small pieces put them into pot and add just enough water to cover them, set to boil.
2. Add salt, allspice, bay leaf and crushed garlic.
3. Once potatoes are boiled DO NOT pour the water out! Instead mix the flour with the cream and add it to the pot, you can add some butter as well.
4. Finally add 2 raw eggs and fried bacon including the fat and stir thoroughly.
5. Serve with the hard boiled egg cut in half, with "zablafuňky" poured over it.

## Contribution

- Zeth - btc: `xpub69HcNNz6Psr4TvRqnfF1mKs3aJr6HBzDznrcC55Gqp3Xzhb9SUWd15qceTeXe1vKHgFUYSUDgrakjc8b2XHj8GJ4ZMXqdfSUF9SfqgSWXUs` [website](https://zethjack.eu) 

;tags: czech potato bacon
